package TowerHanoi;

import java.util.Scanner;
import java.util.Stack;

public class Hanoi {

    private Stack<Integer> col1 = new Stack();
    private Stack<Integer> col2 = new Stack();
    private Stack<Integer> col3 = new Stack();

    public void printAddCol(){
        col1.push(1);
        col1.push(2);
        col1.push(3);
        System.out.println(col1);
        System.out.println(col2);
        System.out.println(col3);
    }
    public void play() {
        int yo = 0;
        System.out.println("From: ");
        Scanner sc = new Scanner(System.in);
        int firstCol = sc.nextInt();
        System.out.println("on: ");
        int secondCol = sc.nextInt();
//        if (firstCol == 1) {
//            yo =  col1.peek();
//            col1.pop();
//        } else if (firstCol == 2) {
//            yo =  col2.peek();
//            col2.pop();
//        } else if (firstCol == 3) {
//            yo =  col3.peek();
//            col3.pop();
//        }
//        if ((secondCol == 1) && ( col1.peek() > yo)) {
//            col1.push(yo);
//        } else if ((secondCol == 2) && ( col2.peek() > yo)) {
//            col2.push(yo);
//        } else if ((secondCol == 3) && ( col3.peek() > yo)) {
//            col3.push(yo);
//        }
        if ((firstCol == 1) && (secondCol == 2) && (col2.empty())){
            changeCol(col1, col2);
        }
        if ((firstCol == 1) && (secondCol == 3) && (col3.empty())){
            changeCol(col1, col3);
        }
        if ((firstCol == 2) && (secondCol == 1) && (col1.empty())){
            changeCol(col2, col1);
        }
        if ((firstCol == 2) && (secondCol == 3) && (col3.empty())){
            changeCol(col2, col3);
        }
        if ((firstCol == 3) && (secondCol == 1) && (col1.empty())){
            changeCol(col3, col1);
        }
        if ((firstCol == 3) && (secondCol == 2) && (col2.empty())){
            changeCol(col3, col2);
        }
    }
     public void changeCol(Stack first, Stack second){
            int e = (int) first.pop();
            second.push(e);
    }
    public void printCol(){
        System.out.println(col1);
        System.out.println(col2);
        System.out.println(col3);
    }
}
