package TowerHanoi;

public class Main {
    public static final int SIZE=3;

    public static void main(String[] args) {
        Hanoi hanoi = new Hanoi();
        hanoi.printAddCol();
        while (true) {

            hanoi.play();
            hanoi.printCol();
        }
    }
}
